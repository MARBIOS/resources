# MARBIOS Documentation. Resources
Presentations, manuals and other useful resources to make your work easier around the 
**Marine Bioinformatics Service** at ICM (MARBIOS, aka Biocuster).  
Browse them [**here**](https://gitlab.com/MARBIOS/resources/tree/master)  
  
- **Read the [Contribution Guide](https://gitlab.com/MARBIOS/biocluster-info/blob/master/CONTRIBUTING.md) 
before collaborating in this repository.**  
- Visit the MARBIOS [**Wiki**](https://gitlab.com/MARBIOS/biocluster-info/wikis/home) 
for detailed information on the Marine Bioinformatics Service, and feel free 
to add new contents.  
- Use the [**Issue Tracker**](https://gitlab.com/MARBIOS/biocluster-info/issues) on the 
main MARBIOS project to post questions, requests, phylosophical problems, etc.  

----

### Contents
- [**Presentations**](https://gitlab.com/MARBIOS/resources/tree/master/Presentations) 
of seminars or TEMAs relevant to biocluster users.  
- List of [**Publications**](https://gitlab.com/MARBIOS/resources/tree/master/Publications) 
by Marine Bioinformatics Service users.  
- [**Logos**](https://gitlab.com/MARBIOS/resources/tree/master/Logos) of the Marbits platform to include in your posters or presentations.  


Help us to improve this site, and edit whatever you fell clould be better!

