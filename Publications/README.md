# Publications directory
Here you can find a [list of Publications](https://gitlab.com/MARBIOS/resources/blob/master/Publications/publications.md) 
where the Marine Bioinformatics Service 
has played a role. You can download the [`bib` version](https://gitlab.com/MARBIOS/resources/blob/master/Publications/publications.bib) for building a bibliography in 
LaTeX or other bibliography management software.  

Please, if you add any item in this list, make sure to update **both files**.  